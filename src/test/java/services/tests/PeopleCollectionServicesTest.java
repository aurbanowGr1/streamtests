package services.tests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import domain.Address;
import domain.Person;
import domain.User;
import services.PeopleCollectionServices;

public class PeopleCollectionServicesTest {

	@Test(expected=NullPointerException.class)
	public void col_services_should_throw_exception_if_null_is_entered_as_a_parameter(){
		PeopleCollectionServices.findUsersWhohaveMoreThanOneAddress(null);
	}

	@Test
	public void col_services_should_return_empty_list_if_there_is_no_result(){
		
		User u = new User();
		Person p = new Person();
		u.setPerson(p);
		List<User> users = new ArrayList<User>();
		users.add(u);
		
		List<User> result = PeopleCollectionServices.findUsersWhohaveMoreThanOneAddress(users);
		
		assertTrue(result.size()==0);
	}
	
	@Test
	public void col_services_should_return_proper_result_if_there_exists_user_with_more_than_1_address()
	{

		User u = new User();
		User userWith2Addresses = new User();
		Person p = new Person();
		Person p2 = new Person();
		Address a1 = new Address();
		Address a2 = new Address();
		Address a3 = new Address();
		p.getAddresses().add(a1);
		p2.getAddresses().add(a3);
		p2.getAddresses().add(a2);
		u.setPerson(p);
		userWith2Addresses.setPerson(p2);
		
		List<User> users = new ArrayList<User>();
		users.add(u);
		users.add(userWith2Addresses);

		List<User> result = PeopleCollectionServices.findUsersWhohaveMoreThanOneAddress(users);
		
		assertTrue(result.size()==1);
		assertSame(result.get(0), userWith2Addresses);
	}
	
	@Test
	public void col_services_should_ignore_users_without_person_details(){
		User u = new User();
		User userWith2Addresses = new User();
		Person p = new Person();
		Person p2 = new Person();
		Address a1 = new Address();
		Address a2 = new Address();
		Address a3 = new Address();
		p.getAddresses().add(a1);
		p2.getAddresses().add(a3);
		p2.getAddresses().add(a2);
		u.setPerson(p);
		User userWithoutPersonDetails = new User();
		userWith2Addresses.setPerson(p2);

		List<User> users = new ArrayList<User>();
		users.add(u);
		users.add(userWith2Addresses);
		users.add(userWithoutPersonDetails);

		List<User> result = PeopleCollectionServices.findUsersWhohaveMoreThanOneAddress(users);

		assertTrue(result.size()==1);
		assertSame(result.get(0), userWith2Addresses);
	}
}
