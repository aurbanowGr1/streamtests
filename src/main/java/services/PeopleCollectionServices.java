package services;

import java.util.ArrayList;
import java.util.List;

import domain.User;

public class PeopleCollectionServices {

	public static List<User> findUsersWhohaveMoreThanOneAddress(List<User> users){
		if(users==null)
			throw new NullPointerException();
		List<User> result = new ArrayList<User>();
		
		for(User user: users){
			if(user.getPerson() != null)
			if(user.getPerson().getAddresses().size()>1)
				result.add(user);
		}
		
		return result;
		
		
	}
	
}
